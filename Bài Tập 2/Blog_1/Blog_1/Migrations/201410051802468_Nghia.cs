namespace Blog_1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nghia : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Commets",
                c => new
                    {
                        Post_Id = c.Int(nullable: false, identity: true),
                        Commet = c.String(),
                    })
                .PrimaryKey(t => t.Post_Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Commets");
        }
    }
}
