﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_1.Models;

namespace Blog_1.Controllers
{
    public class CommetsController : Controller
    {
        private BlogDBContext db = new BlogDBContext();

        //
        // GET: /Commets/

        public ActionResult Index()
        {
            return View(db.Commets.ToList());
        }

        //
        // GET: /Commets/Details/5

        public ActionResult Details(int id = 0)
        {
            Commets commets = db.Commets.Find(id);
            if (commets == null)
            {
                return HttpNotFound();
            }
            return View(commets);
        }

        //
        // GET: /Commets/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Commets/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Commets commets)
        {
            if (ModelState.IsValid)
            {
                db.Commets.Add(commets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(commets);
        }

        //
        // GET: /Commets/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Commets commets = db.Commets.Find(id);
            if (commets == null)
            {
                return HttpNotFound();
            }
            return View(commets);
        }

        //
        // POST: /Commets/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Commets commets)
        {
            if (ModelState.IsValid)
            {
                db.Entry(commets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(commets);
        }

        //
        // GET: /Commets/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Commets commets = db.Commets.Find(id);
            if (commets == null)
            {
                return HttpNotFound();
            }
            return View(commets);
        }

        //
        // POST: /Commets/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Commets commets = db.Commets.Find(id);
            db.Commets.Remove(commets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}