﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "Nhập từ 10-100 ký tự"),
        MinLength(5, ErrorMessage = "Nhập từ 10-100 ký tự")]
        public String Content { set; get; }

        //Tạo quan hệ với Post
        public virtual ICollection<Post> Posts { set; get; }
    }
}