﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required]
        [MinLength(5, ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày tháng DD/MM/YYYY")]
        public DateTime DateCreated { set; get; }

        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày tháng DD/MM/YYYY")]
        public DateTime DateUpdated { set; get; }

        [Required]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        //Tạo quan hệ với Post
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}