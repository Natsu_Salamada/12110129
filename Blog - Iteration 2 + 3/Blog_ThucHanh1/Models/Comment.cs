﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_ThucHanh1.Models
{
    [Table("Bình Luận")]
    public class Comment
    {
        public int Id { set; get; }
         [Required]
         [MinLength(50, ErrorMessage = "Bạn phải nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
         [Required]
         [DataType(DataType.DateTime, ErrorMessage = "Bạn phải nhập đúng định dạng")]
        public DateTime DateCreated { set; get; }
         [Required]
         [DataType(DataType.DateTime, ErrorMessage = "Bạn phải nhập đúng định dạng")]
        public DateTime DateUpdated { set; get; }
        public int PostId { set; get; }
        public virtual Post Post { set; get; }
    }
}