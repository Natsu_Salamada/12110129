﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_ThucHanh1.Models
{
    [Table("Bài Viết")]
    public class Post
    {
       
        public int Id { set; get; }
         [Required]
        [StringLength(500,ErrorMessage="Bạn phải nhập torng khoảng từ 20 đến 500",MinimumLength=20)]
        public String Title { set; get; }
         [Required]
        [MinLength(50,ErrorMessage="Bạn phải nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
         [Required]
        [DataType(DataType.DateTime,ErrorMessage="Bạn phải nhập đúng định dạng")]
        public DateTime DateCreated { set; get; }
         [Required]
         [DataType(DataType.DateTime, ErrorMessage = "Bạn phải nhập đúng định dạng")]
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public int AccountId { set; get; }
        public virtual Account Accounts { set; get; }
    }
}