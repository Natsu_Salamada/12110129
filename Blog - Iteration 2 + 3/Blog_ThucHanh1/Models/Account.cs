﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_ThucHanh1.Models
{
    public class Account
    {
        public int Id { set; get; }
         [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
         [Required]
        [DataType(DataType.EmailAddress,ErrorMessage="Địa chỉ email bạn nhập chưa hợp lệ!")]
        public String Email { set; get; }
         [Required]
        [StringLength(100,ErrorMessage="Bạn đã nhập vượt quá 100 ký tự!")]
        public String FistName { set; get; }
         [Required]
         [StringLength(100, ErrorMessage = "Bạn đã nhập vượt quá 100 ký tự!")]
        public String LastName { set; get; }
        public virtual ICollection<Post> Posts { set; get; }

    }
}