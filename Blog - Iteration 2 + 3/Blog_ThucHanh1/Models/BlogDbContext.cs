﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_ThucHanh1.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(p => p.Tags).WithMany(t => t.Posts).Map(l => l.MapLeftKey("PostId").MapRightKey("TagId").ToTable("Post_Tag"));
        }
        public DbSet<Account> Accounts { set; get; }

        public DbSet<Tag> Tags { get; set; }
    }
}