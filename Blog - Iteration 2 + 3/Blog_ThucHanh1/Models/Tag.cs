﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_ThucHanh1.Models
{
    public class Tag
    {
        public int Id { set; get; }
         [Required]
         [StringLength(100, ErrorMessage = "Bạn phải nhập torng khoảng từ 10 đến 100", MinimumLength = 10)]
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}