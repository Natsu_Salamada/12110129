namespace Blog_ThucHanh1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan_2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Post_Tag",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostId, t.TagId })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post_Tag", new[] { "TagId" });
            DropIndex("dbo.Post_Tag", new[] { "PostId" });
            DropForeignKey("dbo.Post_Tag", "TagId", "dbo.Tags");
            DropForeignKey("dbo.Post_Tag", "PostId", "dbo.Posts");
            DropTable("dbo.Post_Tag");
            DropTable("dbo.Tags");
        }
    }
}
