namespace Blog_ThucHanh1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan_5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FistName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Bài Viết", "AccountId", c => c.Int(nullable: false));
            AddForeignKey("dbo.Bài Viết", "AccountId", "dbo.Accounts", "Id", cascadeDelete: true);
            CreateIndex("dbo.Bài Viết", "AccountId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bài Viết", new[] { "AccountId" });
            DropForeignKey("dbo.Bài Viết", "AccountId", "dbo.Accounts");
            DropColumn("dbo.Bài Viết", "AccountId");
            DropTable("dbo.Accounts");
        }
    }
}
