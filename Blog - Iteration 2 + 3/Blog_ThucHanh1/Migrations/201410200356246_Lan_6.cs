namespace Blog_ThucHanh1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan_6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bài Viết", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Bài Viết", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Bình Luận", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FistName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FistName", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Bình Luận", "Body", c => c.String());
            AlterColumn("dbo.Bài Viết", "Body", c => c.String());
            AlterColumn("dbo.Bài Viết", "Title", c => c.String());
        }
    }
}
