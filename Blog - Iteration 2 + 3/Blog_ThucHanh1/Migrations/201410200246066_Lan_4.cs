namespace Blog_ThucHanh1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan_4 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Comments", newName: "Bình Luận");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Bình Luận", newName: "Comments");
        }
    }
}
