namespace Blog_ThucHanh1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan_3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "Bài Viết");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Bài Viết", newName: "Posts");
        }
    }
}
