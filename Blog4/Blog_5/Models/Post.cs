﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_5.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        //Tạo quan hệ với User đăng bài
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
    }
}