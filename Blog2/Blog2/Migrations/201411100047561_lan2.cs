namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "Body", c => c.String());
            DropColumn("dbo.Comments", "Dody");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "Dody", c => c.String());
            DropColumn("dbo.Comments", "Body");
        }
    }
}
