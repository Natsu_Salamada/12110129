﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }
        [Required]
        public string Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }

        
    }
}